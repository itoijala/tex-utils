build/test-latex23.log: test-latex23.tex
	@mkdir -p build
	max_print_line=1048576 lualatex --interaction=batchmode --output-directory=build test-latex23.tex || true

clean:
	rm -rf build

.PHONY: clean
